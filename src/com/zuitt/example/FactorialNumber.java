package com.zuitt.example;

import java.util.Scanner;
public class FactorialNumber {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);

        System.out.println("Input an integer whose factorial will be computed:");

        int answer = 1;
        int counter = 1;

        try {
            int num = in.nextInt();
            if (num < 0) {
                System.out.println("Cannot compute negative integer.");
                return;
            }

            while (counter <= num) {
                answer *= counter;
                counter++;
            }
            System.out.println("The factorial of " + num + " is " + answer);
        }
        catch (Exception e) {
            System.out.println("Invalid data. You must enter a whole number.");
        }
    }
}